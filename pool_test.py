import os
import numpy as np
from multiprocessing import Pool


def pool_fn(_):
    rng = np.random.default_rng()
    dim = 2**14

    A = rng.random(dim**2).reshape(dim, dim)
    B = rng.random(dim**2).reshape(dim, dim)

    return A * B


def pool_fn2(_):
    rng = np.random.default_rng()
    sz = 2**30

    a = rng.random(sz)
    b = rng.random(sz)

    return a * b


def main():
    """
    lst = []
    with Pool(8) as pool:
        lst.extend(pool.map(pool_fn, (_ for _ in range(0, 64))))

    print(lst)
    """

    print(str(pool_fn(0)))


if __name__ == '__main__':
    main()
