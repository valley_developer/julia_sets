#include <stdbool.h>
#include <stdlib.h>
#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

#define T_COUNT (1)

struct julia_sequence_args {
    double complex *values;
    int num_values;
    double complex base;
    int iterations;
};

bool cisnan(double complex x) {
    return isnan(creal(x)) || isnan(cimag(x));
}

double complex compute_julia_sequence(
    double complex value,
    double complex base,
    int iterations
) {
    double complex x = value;

    for (int i = 0; i < iterations && !cisnan(x); i++) {
        x = (x * x) + base;
    }

    return x;
}

void* julia_set_thread(void* arg) {
    struct julia_sequence_args *args = (struct julia_sequence_args*) arg;
    double complex *values = args->values;
    const int num_values = args->num_values;
    const double complex base = args->base;
    const int iterations = args->iterations;

    for (int i = 0; i < num_values; i++) {
        values[i] = compute_julia_sequence(values[i], base, iterations);
    }

    return NULL;
}

int compute_julia_set(
    double complex *domain,
    int domain_sz,
    double complex base,
    int iterations
) {
    pthread_t thread_pool[T_COUNT] = {};
    int threads_made = 0;
    int thread_create_error = 0;
    int thread_join_error = 0;
    struct julia_sequence_args arg_array[T_COUNT] = {};
    const size_t step_sz = domain_sz / T_COUNT;

    for (int i = 0; (i < T_COUNT) && (thread_create_error == 0); i++) {
        // initialize argument for this pthread
        arg_array[i].values = domain + (i * step_sz);
        arg_array[i].num_values = step_sz;
        arg_array[i].base = base;
        arg_array[i].iterations = iterations;

        // the last thread will handle any residual elements
        if (i == (T_COUNT - 1)) {
            arg_array[i].num_values += domain_sz % T_COUNT;
        }

        // create pthread
        thread_create_error = pthread_create(
            &(thread_pool[i]),
            NULL,
            *julia_set_thread,
            (void*) &(arg_array[i])
        );

        // If we made a thread then increment the thread count
        if (thread_create_error == 0) {
            ++threads_made;
        }
    }

    for (int j = 0; (j < threads_made) && (thread_join_error == 0); j++) {
        thread_join_error = pthread_join(thread_pool[j], NULL);
    }

    return thread_create_error || thread_join_error;
}

double complex* generate_domain(double complex seed, double complex granularity, int horizontal, int vertical) {
    int sz = horizontal * vertical;
    double complex* domain = (double complex*) calloc(sz, sizeof(double complex));

    for (int i = 0; i < horizontal; i++) {
        for (int j = 0; j < vertical; j++) {
            double complex* row = domain + (i * horizontal);
            double complex* element = row + j;

            *element = seed + (j * granularity) - (i * granularity * I);
        }
    }

    return domain;
}

double complex* range_from_domain(double complex* domain, int sz) {
    double complex* range = (double complex*) calloc(sz, sizeof(double complex));
    
    return memcpy(range, domain, sz * sizeof(double complex));
}

int main() {
    int horizontal = 200;
    int vertical = 200;
    int sz = horizontal * vertical;

    double complex* domain = generate_domain(
        -0.1l + 0.1l*I,
        0.001l + 0.0l*I,
        horizontal,
        vertical
    );

    double complex* range = range_from_domain(domain, sz);

    int error = compute_julia_set(range, sz, 1 + 0.1l*I, 1000);
    if (error != 0) {
        exit(error);
    }

    printf("domain|output\n");
    for (int i = 0; i < sz; i++) {
        printf(
            "%f + %fi|%f + %fi\n",
            creal(domain[i]),
            cimag(domain[i]),
            creal(range[i]),
            cimag(range[i])
        );
    }

    free(range);
    free(domain);
    exit(0);
}
