julia: julia.c
	gcc -O3 -Wextra -Wall -o $@ $^ -lpthread

debug: julia.c
	gcc -g -Wextra -Wall -o $@ $^ -lpthread

clean:
	rm -f julia debug
