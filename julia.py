import time
import warnings
import numpy as np
import functools as ft
from multiprocessing import Pool


def generate_julia_set_parallel(
        real_dom,
        imgn_dom,
        jbase,
        iterations=1000,
        pool_sz=8
):
    cmplx_dom = (
            np.repeat(real_dom, len(imgn_dom)),
            + (1j * np.tile(imgn_dom, len(real_dom)))
    )

    with Pool(pool_sz) as pool:
        return np.concatenate(
            pool.map(
                ft.partial(calculate_julia_set, jbase, iterations=iterations),
                (
                    cmplx_dom[i:i + 1024]
                    for i in range(0, len(real_dom), 1024)
                )
            )
        )


def generate_julia_set(real_dom, imgn_dom, jbase, iterations=1000):
    cmplx_dom = (
            np.repeat(real_dom, len(imgn_dom))
            + (1j * np.tile(imgn_dom, len(real_dom)))
    )

    return calculate_julia_set(jbase, cmplx_dom, iterations)


def calculate_julia_set(jbase, cmplx_data, iterations=100):
    return np.nan_to_num(
        ft.reduce(
            lambda c, _: np.add(np.square(c), jbase),
            range(0, iterations),
            cmplx_data
        ),
        nan=np.infty
    )


def main():
    real_range = np.linspace(-2.0, 1.0, 8092)
    imgn_range = np.linspace(-1.0, 1.0, 8092)
    base = 1.0 + 1.0j

    start = time.process_time_ns()
    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', r'overflow encountered in square')
        warnings.filterwarnings('ignore', r'invalid value encountered in square')
        generate_julia_set_parallel(real_range, imgn_range, base)

    end = time.process_time_ns()

    print(
        "start = {start}ns\nend = {end}ns\ntime = {time_ns}ns, {time_ms:.3f}ms".format(
            start=start,
            end=end,
            time_ns=(end - start),
            time_ms=((end - start) / 10**6)
        )
    )


if __name__ == '__main__':
    main()
